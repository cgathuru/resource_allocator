/* Resource Allocator
*	 Charles Gathuru
*	 Student no: 260457189
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <time.h>
#include <stdint.h>

#ifndef max
	#define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef min
	#define min( a, b ) ( ((a) < (b)) ? (a) : (b) )
#endif

int processes, strategy, resources, resourceL, resourceH;
double simulationTime;

int *available_resources;
int *threadWaiting;
int *processTime;

int **current_resources;
int **max_resources;
int **needed_resources;

pthread_t * threadp;
pthread_cond_t condp;
pthread_cond_t condr;
pthread_mutex_t mutex;

typedef struct thread
{
	int id;
	int timeSimulated;
} Thread;

Thread **threads;

clock_t start, end;

void removePossibleDeadlock();
void reallocateResources(int id);

/**
 * Generates a random number within a given range
 * @param  min lower end of the range
 * @param  max upper end if the range
 * @return     The random number between the minimum and maximum.
 */
int generateRandomNumber(int min, int max)
{
	return min + rand() / (RAND_MAX / (max - min + 1) + 1);
}

/**
 * Implements bankers alogrithm using arrays. The alogirthm runs
 * using a predetermined resouce allocation based on a random
 * number generator.
 * @param processes The numbe of processes being simulated
 */
void bankersAlgorithm(int processes)
{
	int count =0;
	int i,j;
	bool safe,executing;
	bool process_runnning[processes];
	/* Initialize all processes to running */
	for (i = 0; i < processes; i++)
	{
		process_runnning[i] = true;
		count++;
	}
	/* Check for an unsafe state */
	while(count != 0)
	{
		safe = false;
		for (i = 0; i < processes; i++)
		{
			if(process_runnning[i])
			{
				executing = true;
				for (j = 0; j < resources; j++)
				{
					if (needed_resources[i][j] > available_resources[i])
					{
						executing = false;
						break;
					}
					if (executing)
					{
						printf("Process %d is executing\n", i);
						sleep(1);
						processTime[i] += 1;
						process_runnning[i] = false;
						count--;
						safe = true;

						for (j = 0; j < resources; j++)
						{
							available_resources[j] += current_resources[i][j];
							current_resources[i][j] = 0;
						}
						break;
					}
				}
			}
		}
		if (!safe)
		{
			printf("The processes are in an unsafe sate\n");
			break;
		}
		else
		{
			printf("The process is in a safe state\n");
			printf("Available resources\n");

			for (i = 0; i < resources; i++)
			{
				printf("\t%d", available_resources[i]);
			}
			printf("\n");
		}
	}
}

/**
 * Enables protection of process counter that determines the
 * allocation of resources i.e. whether a resources will keep
 * or realese the resources that it has
 * @param processCounter The number of processes that are not waiting
 * @param threadno       The thread id of the current thread
 * @param indicator      The flag to protect the process counter variable
 * @param processes      The total number of processes being simulated
 */
void processCheck(int *processCounter, int threadno, bool *indicator, int processes)
{
	/* Check if last resource */
	bool flag = *indicator;
	if (flag)
	{
		return;
	}
	else
	{
		flag = true;
		*processCounter-=1; //decrement the counter
	}

	int i;

	/* If this is the only resource left then relinqush the currently
	 * held resources */
	if(*processCounter == 0)
	{
		for (i = 0; i < resources; i++)
		{
			available_resources[i] += current_resources[threadno][i];
			needed_resources[threadno][i] +=  current_resources[threadno][i];
			current_resources[threadno][i] = 0;
			*processCounter =  processes; // reset the process counter
		}
	}
}

/**
 * Generates a new request for resources
 * @param id The id number corresponding to the process
 */
void reallocateResources(int id)
{
	int i;
	for (i = 0; i < resources; i++)
	{
		current_resources[id][i] = generateRandomNumber(0, max_resources[id][i]);
		available_resources[i]  = generateRandomNumber(resourceL, max_resources[id][i]);
		needed_resources[id][i] = max_resources[id][i] - current_resources[id][i];
	}
}

/**
 * Execute the process of deadlock detection
 * @param ptr A pointer with infomration that the function requires
 */
void* execute(void *ptr)
{
	int i,counter=processes;
	int *processCounter;
	processCounter = &counter;
	double timeElapsed;
	bool running = true;
	while(running)
	{
		pthread_mutex_lock(&mutex); //lock
		//int id = (intptr_t) ptr; //get the thread id
		Thread *currentThread = (Thread *) ptr;
		int id = (*currentThread).id;

		bool executed = true;
		for (i = 0; i < resources; i++)
		{
			if(needed_resources[id][i] != 0)
			{
				executed = false;
			}
		}
		if (executed)
		{
			reallocateResources(id);
			removePossibleDeadlock();
			pthread_cond_signal(&condp);
			pthread_mutex_unlock(&mutex);
		}

		bool flag = false;
		for (i = 0; i < resources; i++)
		{
			while (needed_resources[id][i] > available_resources[i])
			{
				processCheck(processCounter, id, &flag, processes);
				/* Wait until there are more resources */
				printf("Thread %d waiting\n", id);
				pthread_cond_signal(&condp);
				pthread_cond_wait(&condp, &mutex);
			}
		}
		flag = false;
		printf("Process %d executing...\n", id);
		sleep(1); //simulate process executing
		printf("Process has %d finished executing\n", id);
		timeElapsed =1;
		(*currentThread).timeSimulated += timeElapsed;
		/* Release resources after executing */
		for (i = 0; i < resources; i++)
		{
			available_resources[i] += current_resources[id][i];
			needed_resources[id][i] = 0;
			current_resources[id][i] = 0;
		}
		printf("Process %d has released its resources\n", id );
		pthread_cond_signal(&condp); //signal more resources available
		pthread_mutex_unlock(&mutex); //unlock
	}
	exit(0);
}

/**
 * Removes any deadlock that can occur by checking the allocation
 * of resources to see check if n-1 process run, would there be
 * enough resources including the available resource for the nth
 * process to exectue.
 */
void removePossibleDeadlock()
{
	int i,j;

	/* Check that the availables resources + sum all resources
	 * > maximum resources */
	int *max, *sum, *maxValue;
	/* Stores the max value */
	max = malloc ( resources * sizeof(int));
	/* Stores the summation of current resources excluding the
	* maximum needed resources request */
	sum = malloc (resources * sizeof (int)); // Stores the sum
	/* Stores the index of the maximum value */
	maxValue = malloc (resources * sizeof(int));

	for (i = 0; i < resources; i++)
	{
		max[i] = available_resources[i];
		sum[i] = 0;
		maxValue[i] = 0;
	}
	/* Calculate the maximum value for a resouce type for a process */
	for (i = 0; i < processes; i++)
	{
		for (j = 0; j < resources; j++)
		{
			max[j] = max(max[j], needed_resources[i][j]);
			maxValue[i] = j;
		}
	}
	/* Sum of resouces of same type execpt the largest value */
	for (i = 0; i < processes; i++)
	{
		for (j = 0; j < resources; j++)
		{
			if (j != maxValue[j])
			{
				sum[i] += needed_resources[i][j];
			}
		}
	}
	/* Modify allocation of resouces if deadlock can occur */
	for (i = 0; i < resources; i++)
	{
		if (((max[i] - sum[i]) > 0 ) && (max[i] > available_resources[i]))
		{
			available_resources[i] += (max[i] - sum[i]); // correct resource allocation
		}
	}
	free(max);
	free(sum);
	free(maxValue);
}

/**
 * Detects the deadlock and changes modifies the allocation of resources
 * to ensure that deadlock cannot error.
 * @param processes The number of processes.
 */
void deadlockDetection(int processes)
{
	pthread_mutex_init(&mutex,NULL); //Initialize the mutex
  pthread_cond_init(&condp,NULL); // Initialize the processes conditional variable
  pthread_cond_init(&condr, NULL); // Initialize resource alocator conditional variable
	int i;
	//threadWaiting = malloc(processes * sizeof(int));

	/* Create the Thread objects */
	threads = malloc(sizeof(Thread *) * processes);
	for (i = 0; i < processes; i++)
	{
		threads[i] = malloc( sizeof(Thread) * processes);
	}

	/* Create the process threads */
	pthread_t * threadp = malloc(sizeof(pthread_t) * processes);
	for (i = 0; i < processes; i++)
	{
		(*threads[i]).id = i;
		pthread_create(&threadp[i], NULL, execute,(void*) threads[i] );
	}
	sleep(simulationTime);
	/* Cancel the threads after the simulation time has finished */
	for(i = 0; i< processes; i++)
	{
	    pthread_cancel(threadp[i]);
	}
	printf("Running cleanup\n");
	/* Clean up */
	pthread_mutex_destroy(&mutex);
}

/**
 * Frees the memory that was allocated for max resouces,
 * allocated resources and current resources as well as
 * the threads.
 */
void freeMemory()
{
	int i;
	for (i = 0; i < processes; i++)
	{
		free(max_resources[i]);
		free(current_resources[i]);
		free(needed_resources[i]);
	}
	free(max_resources);
	free(current_resources);
	free(needed_resources);
	free(available_resources);
	free(threads);
}

/**
 * Inititalises the memory and does initial allocation of resouces for
 * each reasource type.
 */
void createResources()
{
	int i;
	int j;

	/*  Create the desired max number of resources */
	max_resources = malloc(processes * sizeof(int *));

	/* create a random number of resources within specified resources
	 range */
	for(i=0; i < processes; i++)
	{
		max_resources[i] = malloc(resources * sizeof(int));
		for (j = 0; j < resources; j++)
		{
			int random = generateRandomNumber(resourceL,resourceH);
			max_resources[i][j] = random;
			printf("Process %d has a maximum number of %d resources for resource %d\n",i,max_resources[i][j],j);
		}
	}

	/* Create a random number of currently used resources */
	current_resources = malloc( processes * sizeof(int *));
	for (i = 0; i < processes; i++)
	{
		current_resources[i] = malloc (resources * sizeof (int));
		for(j=0; j < resources; j++)
		{
			/* Create process with resource less than maximum */
			int random = generateRandomNumber(0, max_resources[i][j]);
			current_resources[i][j] = random;
			printf("Process %d currently has %d resources of resource %d\n",i,current_resources[i][j],j);
		}
	}

	/* Calculate the available resources */
	available_resources = malloc (resources * sizeof(int));
	for (i = 0; i < resources; i++)
	{
		available_resources[i] =  generateRandomNumber(resourceL + 1, resourceH);
		printf("Resource %d has %d available resources\n",i,available_resources[i] );
	}

	/* Calculate the needed resources for each process */
	needed_resources = malloc( processes * sizeof(int*));
	for (i = 0; i < processes; i++)
	{
		needed_resources[i] = malloc (resources * sizeof(int));
		for (j = 0; j < resources; j++)
		{
			needed_resources[i][j] = max_resources[i][j] - current_resources[i][j];
			printf("Process %d needs %d of resource %d\n",i,needed_resources[i][j],j );
		}
	}

}

/**
 * Allocates more resources for bannkers alogrithm to be run again
 */
void allocateMoreResouces()
{
	int i,j;
	for (i = 0; i < processes; i++)
	{
		for (j = 0; j < resources; j++)
		{
			max_resources[i][j] = generateRandomNumber(resourceL, resourceH);
			current_resources[i][j] = generateRandomNumber(0, max_resources[i][j]);
			available_resources[i] =  generateRandomNumber(resourceL + 1, resourceH);
			needed_resources[i][j] = max_resources[i][j] - current_resources[i][j];
		}
	}
}

/**
 * Runs bankers algorithm until the simulation time as run out
 * @param  ptr Pointer to intput data
 * @return     Exit data
 */
void * runBankers(void *ptr)
{
	while(true)
	{
		bankersAlgorithm(processes);
		allocateMoreResouces();
	}
}

int main(int argc, char* argv[])
{

	strategy = atoi(argv[1]); // 1 is bankers and 2 is deadlock avoidance
	processes = atoi(argv[2]); // Number of processes that should be simulated
	resources = atoi(argv[3]); // The types of resources
	resourceL = atoi(argv[4]); // The lower end  number of resources
	resourceH = atoi(argv[5]); // The higher end number of resources
	simulationTime = atoi(argv[6]); //The amount of time the simlation should run

	/*processes = 3;
	resources = 3;
	resourceL = 3;
	resourceH = 8;
	strategy = 2;
	simulationTime = 10;*/

	srand((unsigned)time(NULL)); //Seed the random number generator.

	createResources();
	if(strategy == 1)
	{
		int i;
		processTime = malloc( processes * sizeof(int));
		for (i = 0; i < processes; i++)
		{
			processTime[i] = 0;
		}
		pthread_t bankers;
		pthread_create( &bankers, NULL, &runBankers, NULL);
		sleep(simulationTime);
		pthread_cancel(bankers);
		for (i = 0; i < processes; i++)
		{
			printf("Process %i executed for %i seconds \n", i, processTime[i]);
		}
		free(processTime);
	}
	else
	{
		removePossibleDeadlock();
		deadlockDetection(processes);
		int i;
		for (i = 0; i < processes; i++)
		{
			printf("Process %i executed for %d seconds\n", i, (*threads[i]).timeSimulated);
		}
	}
	freeMemory();
	return EXIT_SUCCESS;
}
