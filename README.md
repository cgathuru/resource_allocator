#README

##Charles Gathuru
##Student no: 260457819

The program allocates resources based on a random number generator.
The resource range specified in the command inputs for the program
indicate the range the the maximum number of resources each resource
type should have. Each resource type will have a different maximum
number of resources. The available resources are calculated based on
the amount of resources being used by the processes.

The file should be executed as follows. Compile with gcc and execute
./a.out <strategy> <processes> <resources> <resourceL> <resourceH>
<simulationTime>.

<strategy> The strategy to be implemented. 1 would be banker's
algorithm and 2 would be deadlock avoidance
<processes> The number of processes to be used
<resources> The types of resources that should be available
<resourceL> The lower end of the resource range
<resourceH> The higher end of the resource range
<simulationTime> The amount of time the algorithm should be simulated
for.

It is assumed that executing for a process will take a second, so the
simulation time is given in seconds and so will the time simulated
by each of the resources. The execution of a process is simulated by
a sleep of 1 second. It is assumed that the time not executing the
process is negligbile so the time per execution can simply be 1 sec.

Bankers Alogrithm is implement using arrays and a for loop, so the
simulation time for each process is equal, however it is different
for the deadlock detection algorithm due to its random nature.
